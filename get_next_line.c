/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/01 14:54:27 by azhadan           #+#    #+#             */
/*   Updated: 2023/05/07 18:17:38 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*get_next_line(int fd)
{
	char		*str;
	static char	buffer[BUFFER_SIZE + 1];
	int			i;

	if (read(fd, 0, 0) < 0 || BUFFER_SIZE <= 0)
	{
		i = 0;
		while (buffer[i])
			buffer[i++] = '\0';
		return (NULL);
	}
	str = NULL;
	while (buffer[0] || read(fd, buffer, BUFFER_SIZE) > 0)
	{
		str = ft_gnl_join(str, buffer);
		if (ft_check(buffer))
			break ;
	}
	return (str);
}

// int	main(void)
// {
// 	int		fd;
// 	char	*out_put;
// 	int		i;

// 	fd = open("get_next_line_utils.c", O_RDONLY);
// 	i = 2;
// 	while (i > 0)
// 	{
// 		out_put = get_next_line(fd);
// 		if (out_put)
// 			printf("%s\n", out_put);
// 		i--;
// 	}
// }
